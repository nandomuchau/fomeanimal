package com.muchau.fomeanimal.widget;

import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

/**
 * Created by Luis F. Muchau on 10/12/2018.
 */
public class FomeAnimalIntentService extends IntentService {

    public static String ACTIVITY_FAVORITE_LIST = "activity_favorite_list";

    public FomeAnimalIntentService() {
        super("FomeAnimalIntentService");
    }

    public static void startWidgetService(Context context, ArrayList<String> remoteIngredientsList) {
        Intent intent = new Intent(context, FomeAnimalIntentService.class);
        intent.putExtra(ACTIVITY_FAVORITE_LIST, remoteIngredientsList);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            ArrayList<String> remoteList = intent.getExtras()
                    .getStringArrayList(ACTIVITY_FAVORITE_LIST);
            handleActionUpdateWidgets(remoteList);
        }
    }

    private void handleActionUpdateWidgets(ArrayList<String> remoteList) {
        Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent.setComponent(new ComponentName(this, FomeAnimalWidgetProvider.class));
        intent.putExtra(ACTIVITY_FAVORITE_LIST, remoteList);
        this.sendBroadcast(intent);
    }
}