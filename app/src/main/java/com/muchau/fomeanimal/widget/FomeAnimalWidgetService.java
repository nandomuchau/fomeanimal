package com.muchau.fomeanimal.widget;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.muchau.fomeanimal.R;

import java.util.ArrayList;
import java.util.List;

import static com.muchau.fomeanimal.widget.FomeAnimalWidgetProvider.favorites;

/**
 * Created by Luis F. Muchau on 10/9/2018.
 */
public class FomeAnimalWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new FomeAnimalViewsFactory(this.getApplicationContext(), intent);
    }
}

class FomeAnimalViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private List<String> mWidgetItems = new ArrayList<String>();

    private Context mContext;

    public FomeAnimalViewsFactory(Context context, Intent intent) {
        mContext = context;
    }

    public void onCreate() {

    }

    public void onDestroy() {
        mWidgetItems.clear();
    }

    @Override
    public int getCount() {
        return mWidgetItems.size();
    }

    public RemoteViews getViewAt(int position) {
        RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_item);
        rv.setTextViewText(R.id.widget_item, mWidgetItems.get(position));

        Bundle extras = new Bundle();
        extras.putInt(FomeAnimalWidgetProvider.EXTRA_ITEM, position);
        Intent fillInIntent = new Intent();
        fillInIntent.putExtras(extras);
        rv.setOnClickFillInIntent(R.id.widget_item, fillInIntent);

        return rv;
    }

    public RemoteViews getLoadingView() {
        return null;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public long getItemId(int position) {
        return position;
    }

    public boolean hasStableIds() {
        return true;
    }

    public void onDataSetChanged() {
        mWidgetItems = favorites;
    }
}
