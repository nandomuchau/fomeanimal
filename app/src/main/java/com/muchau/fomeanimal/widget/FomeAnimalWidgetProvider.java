package com.muchau.fomeanimal.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.muchau.fomeanimal.R;
import com.muchau.fomeanimal.activity.FavoritesActivity;

import java.util.ArrayList;

import static com.muchau.fomeanimal.widget.FomeAnimalIntentService.ACTIVITY_FAVORITE_LIST;

/**
 * Implementation of App Widget functionality.
 */
public class FomeAnimalWidgetProvider extends AppWidgetProvider {

    public static final String TOAST_ACTION = "com.muchau.fomeanimal.widget.TOAST_ACTION";

    public static final String EXTRA_ITEM = "com.muchau.fomeanimal.widget.EXTRA_ITEM";

    static ArrayList<String> favorites = new ArrayList<>();

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(
                new ComponentName(context, FomeAnimalWidgetProvider.class));

        final String action = intent.getAction();

        if (action.equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
            favorites = intent.getExtras().getStringArrayList(ACTIVITY_FAVORITE_LIST);
            if (favorites != null) {
                appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.widget_grid_view);

                //Now update all widgets
                FomeAnimalWidgetProvider.updateWidgets(context, appWidgetManager, appWidgetIds);
            }
        }
        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        /*for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }*/
    }

    public static void updateWidgets(Context context, AppWidgetManager appWidgetManager,
                                           int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {


        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_layout);

        // Call the activity when widget is clicked,
        // but resume activity from stack so you do not pass intent.extras
        Intent appIntent = new Intent(context, FavoritesActivity.class);
        appIntent.addCategory(Intent.ACTION_MAIN);
        appIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        appIntent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT|Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent appPendingIntent = PendingIntent
                .getActivity(context, 0, appIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setPendingIntentTemplate(R.id.widget_grid_view, appPendingIntent);

        views.setEmptyView(R.id.widget_grid_view, R.id.empty_view);

        // Set the WidgetRemoteViewsService intent to act as the adapter for the GridView
        Intent intent = new Intent(context, FomeAnimalWidgetService.class);
        views.setRemoteAdapter(R.id.widget_grid_view, intent);

        appWidgetManager.updateAppWidget(appWidgetId, views);
    }
}

