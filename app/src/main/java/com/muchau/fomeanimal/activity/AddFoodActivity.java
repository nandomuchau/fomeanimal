package com.muchau.fomeanimal.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.muchau.fomeanimal.R;
import com.muchau.fomeanimal.model.Food;
import com.muchau.fomeanimal.model.FoodRecommendation;
import com.muchau.fomeanimal.model.Restaurant;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddFoodActivity extends AppCompatActivity
        implements EventListener<DocumentSnapshot> {

    private static final String TAG = "AddFoodActivity";

    @BindView(R.id.photoPickerButton)
    LinearLayout mPhotoPickerView;

    @BindView(R.id.cameraButton)
    LinearLayout mCameraView;

    @BindView(R.id.foodPictureImageView)
    ImageView mFoodPictureImageView;

    @BindView(R.id.foodNameEditText)
    EditText mFoodNameEditText;

    @BindView(R.id.foodCategoryEditText)
    EditText mFoodCategoryEditText;

    @BindView(R.id.foodDescriptionEditText)
    EditText mFoodDescriptionEditText;

    @BindView(R.id.restaurantNameEditText)
    EditText mRestaurantNameEditText;

    @BindView(R.id.restaurantAddressEditText)
    EditText mRestaurantAddressEditText;

    @BindView(R.id.saveButton)
    Button mSaveButton;

    private FirebaseFirestore mFirestore;

    private CollectionReference mRecommendationsRef;

    private StorageReference mStorageReference;

    private static final int PHOTO_PICKER = 1;

    private static final int REQUEST_IMAGE_CAPTURE = 2;

    private Uri filePath;

    private String mCurrentPhotoPath;

    private String randomUUID;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food);
        ButterKnife.bind(this);

        // Initialize Firestore
        mFirestore = FirebaseFirestore.getInstance();

        // Get reference to the recommendations
        mRecommendationsRef = mFirestore.collection("recommendations");

        // Initialize storage
        FirebaseStorage mStorage = FirebaseStorage.getInstance();
        mStorageReference = mStorage.getReference();

        initPhotoPicker();
        initCamera();
        initGoogleSearch();

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewRecommendation();
            }
        });
    }

    private void initGoogleSearch() {
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                .setCountry("US")
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ESTABLISHMENT).build();

        autocompleteFragment.setFilter(autocompleteFilter);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.i(TAG, "Place: " + place.getName());
                mRestaurantNameEditText.setText(place.getName());
                mRestaurantAddressEditText.setText(place.getAddress());

                // Validate Address
                validateAddress(place.getName().toString(), place.getAddress().toString());
            }

            @Override
            public void onError(Status status) {
                Log.i(TAG, "An error occurred: " + status);
            }
        });
    }

    private Restaurant validateAddress(String name, String address) {
        Restaurant restaurant = new Restaurant();

        String[] addressArray = address.split(",");
        restaurant.setName(name);
        restaurant.setFullAddress(address);
        try {
            restaurant.setAddress(addressArray[0].trim());
            restaurant.setCity(addressArray[1].trim());
            restaurant.setState(addressArray[2].trim().split(" ")[0]);
            restaurant.setZipCode(addressArray[2].trim().split(" ")[1]);
            restaurant.setCountry(addressArray[3].trim());
        } catch (ArrayIndexOutOfBoundsException e) {
            Log.e(TAG, "Error saveRecommendation address", e);
            mRestaurantAddressEditText.setText("");
            Snackbar.make(findViewById(R.id.add_food_view), R.string.address_error_text, Snackbar.LENGTH_SHORT).show();
        }
        return restaurant;
    }

    private void initPhotoPicker() {
        mPhotoPickerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(Intent.createChooser(
                        photoPickerIntent, "Select Picture"), PHOTO_PICKER);
            }
        });
    }

    private void initCamera() {
        mCameraView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        // Error occurred while creating the File
                        Toast.makeText(getApplicationContext(),
                                "Photo file can't be created, please try again",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),
                                "com.muchau.fomeanimal.fileprovider",
                                photoFile);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case PHOTO_PICKER:
                    filePath = data.getData();
                    break;
                case REQUEST_IMAGE_CAPTURE:
                    File imgFile = new File(mCurrentPhotoPath);
                    if (imgFile.exists()) {
                        filePath = Uri.fromFile(imgFile);
                    }
                    break;
            }
            if (filePath != null) {
                try {
                    Bitmap currentImage = MediaStore.Images.Media.getBitmap(
                            this.getContentResolver(), filePath);
                    loadImage(currentImage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void loadImage(Bitmap image) {
        // Load image
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, stream);

        Glide.with(mFoodPictureImageView.getContext())
                .load(stream.toByteArray())
                .asBitmap()
                .error(R.drawable.error)
                .placeholder(R.drawable.placeholder)
                .into(mFoodPictureImageView);
    }

    private void addNewRecommendation() {
        String foodName = mFoodNameEditText.getText().toString();
        String RestaurantName = mRestaurantAddressEditText.getText().toString();
        if (filePath == null || foodName.equals("") || RestaurantName.equals("")) {
            Snackbar.make(findViewById(R.id.add_food_view), R.string.required_fields_error_text, Snackbar.LENGTH_LONG).show();
        } else {
            uploadImage();
        }
    }

    private void uploadImage() {

        if (filePath != null) {
            randomUUID = UUID.randomUUID().toString();
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setTitle("Uploading...");
            mProgressDialog.show();

            StorageReference ref = mStorageReference.child("images/" + randomUUID);
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @SuppressWarnings("StatementWithEmptyBody")
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            mProgressDialog.dismiss();
                            // Get the image Url
                            Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                            while (!urlTask.isSuccessful()) ;
                            Uri downloadUrl = urlTask.getResult();
                            if (downloadUrl != null) {
                                saveRecommendation(downloadUrl.toString());
                            }
                            Toast.makeText(getApplicationContext(), "Uploaded", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            mProgressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            randomUUID = "";
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            mProgressDialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        }
    }

    private void saveRecommendation(String imagePath) {
        Restaurant restaurant = validateAddress(mRestaurantNameEditText.getText().toString(),
                mRestaurantAddressEditText.getText().toString());

        String location = restaurant.getCity() + ", " + restaurant.getState() + ", " + restaurant.getCountry();
        Food food = new Food(
                mFoodNameEditText.getText().toString(),
                mFoodCategoryEditText.getText().toString(),
                mFoodDescriptionEditText.getText().toString(),
                imagePath,
                1,
                location,
                restaurant,
                Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser())
        );

        mRecommendationsRef.add(food)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "Recommendation added. ID: " + documentReference.getId());
                        saveFoodRecommendation(documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding food document", e);
                    }
                });
    }

    private void saveFoodRecommendation(String foodId) {
        CollectionReference foodRecommendationsRef = mFirestore
                .collection("recommendations")
                .document(foodId)
                .collection("foodRecommendations");

        FoodRecommendation foodRecommendation = new FoodRecommendation(
                Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()));

        foodRecommendationsRef.add(foodRecommendation)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "Food Recommendations added. ID: " + documentReference.getId());
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding foodRecommendation document", e);
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot,
                        @javax.annotation.Nullable FirebaseFirestoreException e) {
        if (e != null) {
            Log.w(TAG, "food:onEvent", e);
        }
    }
}
