package com.muchau.fomeanimal.activity;

import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.muchau.fomeanimal.R;
import com.muchau.fomeanimal.model.Favorite;
import com.muchau.fomeanimal.model.Food;
import com.muchau.fomeanimal.model.FoodRecommendation;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity implements EventListener<DocumentSnapshot>,
        OnMapReadyCallback {

    private static final String TAG = "DetailActivity";

    public static final String KEY_FOOD_ID = "key_food_id";

    @BindView(R.id.foodPictureImageView)
    ImageView mFoodPictureImageView;

    @BindView(R.id.favoriteImageView)
    ImageView mFavoriteImageView;

    @BindView(R.id.recommendButton)
    Button mRecommendButton;

    @BindView(R.id.totalRecommendationsTextView)
    TextView mTotalRecommendationsTextView;

    @BindView(R.id.recommendView)
    LinearLayout mRecommendView;

    @BindView(R.id.foodNameTextView)
    TextView mFoodNameTextView;

    @BindView(R.id.foodCategoryTextView)
    TextView mFoodCategoryTextView;

    @BindView(R.id.foodDescriptionTextView2)
    TextView mFoodDescriptionTextView;

    @BindView(R.id.restaurantNameTextView)
    TextView mRestaurantNameTextView;

    @BindView(R.id.restaurantAddressTextView)
    TextView mRestaurantAddressTextView;

    private FirebaseFirestore mFirestore;

    private DocumentReference mRecommendationRef;

    private ListenerRegistration mFoodRegistration;

    private CollectionReference mFoodRecommendationsRef;

    private DocumentReference mFavoriteRef;

    private CollectionReference mFavoritesRef;

    private String mFoodId;

    private String mName;

    private String mImage;

    private String mFavoriteId;

    private FirebaseUser mUserLogged;

    private int mTotalRecommendations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        // Get food ID from extras
        mFoodId = Objects.requireNonNull(getIntent().getExtras()).getString(KEY_FOOD_ID);
        if (mFoodId == null) {
            throw new IllegalArgumentException("Must pass extra " + KEY_FOOD_ID);
        }
        // Get logged user ID
        mUserLogged = FirebaseAuth.getInstance().getCurrentUser();

        // Initialize Firestore
        mFirestore = FirebaseFirestore.getInstance();

        // Get reference to the recommendations
        mRecommendationRef = mFirestore.collection("recommendations").document(mFoodId);
        mFoodRecommendationsRef = mFirestore.collection("recommendations")
                .document(mFoodId).collection("foodRecommendations");

        registerFavoriteClick();

        registerRecommendButton();
    }

    @Override
    public void onStart() {
        super.onStart();

        mFoodRegistration = mRecommendationRef.addSnapshotListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mFoodRegistration != null) {
            mFoodRegistration.remove();
            mFoodRegistration = null;
        }
    }

    @Override
    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
        if (e != null) {
            Log.w(TAG, "food:onEvent", e);
            return;
        }

        if (documentSnapshot != null && documentSnapshot.toObject(Food.class) != null) {
            onRecommendationLoaded(Objects.requireNonNull(documentSnapshot.toObject(Food.class)));
        }
    }

    private void registerFavoriteClick() {
        mFavoriteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Favorite favorite = new Favorite(mFoodId, mName, mImage, mUserLogged);
                if (mFavoriteId != null) {
                    mFavoriteRef = mFirestore.collection("favorites").document(mFavoriteId);
                    mFavoriteRef.delete();
                    mFavoriteId = null;
                    changeFavoriteIconState();
                } else {
                    mFavoritesRef.add(favorite)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    Log.d(TAG, "Recommendation added, ID: " + documentReference.getId());
                                    mFavoriteId = documentReference.getId();
                                    changeFavoriteIconState();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error adding document", e);
                                }
                            });
                }

            }
        });
    }

    private void changeFavoriteIconState() {
        if (mFavoriteId != null) {
            mFavoriteImageView.setImageResource(R.drawable.ic_favorite_24px);
        } else {
            mFavoriteImageView.setImageResource(R.drawable.ic_favorite_border_24px);
        }
    }

    private void registerRecommendButton() {
        mRecommendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                FoodRecommendation foodRecommendation = new FoodRecommendation(
                        Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()));

                mFoodRecommendationsRef.add(foodRecommendation)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d(TAG, "Food Recommendations added. ID: " + documentReference.getId());
                                Snackbar.make(view, R.string.thankyou_recommendation_text, Snackbar.LENGTH_SHORT).show();
                                mRecommendView.removeAllViews();
                                Map<String, Object> updateData = new HashMap<>();
                                updateData.put("recommendations", mTotalRecommendations + 1);
                                mRecommendationRef.update(updateData);
                                getAllRecommendations();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Error adding foodRecommendation document", e);
                            }
                        });
            }
        });
    }

    private void getFavorite() {
        mFavoritesRef = mFirestore.collection("favorites");

        Query query = mFavoritesRef
                .whereEqualTo("foodId", mFoodId)
                .whereEqualTo("userId", mUserLogged.getUid());

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                        mFavoriteId = document.getId();
                    }
                } else {
                    Log.w(TAG, "getFavorite Error");
                }
                changeFavoriteIconState();
            }
        });

    }

    private void onRecommendationLoaded(Food food) {
        mName = food.getName();
        mImage = food.getImage();

        mFoodNameTextView.setText(mName);
        mFoodCategoryTextView.setText(food.getCategory());
        mFoodDescriptionTextView.setText(food.getDescription());
        mRestaurantNameTextView.setText(food.getRestaurant().getName());
        mRestaurantAddressTextView.setText(food.getRestaurant().getFullAddress());

        // Background image
        Glide.with(mFoodPictureImageView.getContext())
                .load(mImage)
                .into(mFoodPictureImageView);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        getFavorite();
        getAllRecommendations();
        getMyFoodRecommendation();
    }

    private void getAllRecommendations() {
        mFoodRecommendationsRef.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                int total = 0;
                if (task.isSuccessful()) {
                    if (task.getResult() != null) {
                        total = task.getResult().size();
                    }
                } else {
                    Log.w(TAG, "getAllRecommendations Error");
                }
                mTotalRecommendations = total;
                setTotalRecommendation();
            }
        });
    }

    private void getMyFoodRecommendation() {
        Query query = mFoodRecommendationsRef
                .whereEqualTo("userId", mUserLogged.getUid());

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                        mRecommendView.removeAllViews();
                    }
                } else {
                    Log.w(TAG, "getMyFoodRecommendation Error");
                }
                changeFavoriteIconState();
            }
        });
    }

    private void setTotalRecommendation() {
        String label;
        if (mTotalRecommendations > 1) {
            label = getResources().getString(R.string.recommendations);
        } else {
            label = getResources().getString(R.string.recommendation);
        }
        mTotalRecommendationsTextView.setText(Integer.toString(mTotalRecommendations) + " " + label);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker and move the camera.
        LatLng latlng = getLocationFromAddress(mRestaurantAddressTextView.getText().toString());
        if (latlng != null) {
            googleMap.addMarker(new MarkerOptions().position(latlng).title(
                    mRestaurantNameTextView.getText().toString()));
            googleMap.setMinZoomPreference(12);
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
        }
    }

    public LatLng getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(this);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return p1;
    }
}
