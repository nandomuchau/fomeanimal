package com.muchau.fomeanimal.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.muchau.fomeanimal.model.Filters;
import com.muchau.fomeanimal.R;
import com.muchau.fomeanimal.adapter.FavoriteAdapter;
import com.muchau.fomeanimal.model.Favorite;
import com.muchau.fomeanimal.model.ActivityViewModel;
import com.muchau.fomeanimal.widget.FomeAnimalIntentService;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoritesActivity extends AppCompatActivity implements FavoriteAdapter.OnFavoriteSelectedListener {

    private static final String TAG = "FavoritesActivity";

    private FirebaseFirestore mFirestore;

    private Query mQuery;

    private FavoriteAdapter mAdapter;

    private ActivityViewModel mViewModel;

    @BindView(R.id.recycler_favorites)
    RecyclerView mFavoritesRecycler;

    @BindView(R.id.view_empty)
    ViewGroup mEmptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        ButterKnife.bind(this);

        // View model
        mViewModel = ViewModelProviders.of(this).get(ActivityViewModel.class);

        // Enable Firestore logging
        FirebaseFirestore.setLoggingEnabled(true);

        initFirestore();
        initRecyclerView();
    }

    private void initFirestore() {
        mFirestore = FirebaseFirestore.getInstance();

        mQuery = mFirestore.collection("favorites")
                .whereEqualTo("userId", Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                .orderBy("timestamp", Query.Direction.DESCENDING);
    }

    private void initRecyclerView() {
        if (mQuery == null) {
            Log.w(TAG, "No query, not initializing RecyclerView");
        }
        int numberOfColumns = calculateNoOfColumns(this);

        mAdapter = new FavoriteAdapter(mQuery, this) {

            @Override
            protected void onDataChanged() {
                // Show/hide content if the query returns empty.
                if (getItemCount() == 0) {
                    mFavoritesRecycler.setVisibility(View.GONE);
                    mEmptyView.setVisibility(View.VISIBLE);
                } else {
                    mFavoritesRecycler.setVisibility(View.VISIBLE);
                    mEmptyView.setVisibility(View.GONE);
                }
            }

            @Override
            protected void onError(FirebaseFirestoreException e) {
                // Show a snackbar on errors
                Snackbar.make(findViewById(android.R.id.content),
                        "Error: check logs for info.", Snackbar.LENGTH_LONG).show();
            }
        };

        mFavoritesRecycler.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        mFavoritesRecycler.setAdapter(mAdapter);
    }

    private void updateWidgetData(ArrayList<String> favorites) {
        FomeAnimalIntentService.startWidgetService(this, favorites);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Apply filters
        onFilter(mViewModel.getFilters());


        // Start listening for Firestore updates
        if (mAdapter != null) {
            mAdapter.startListening();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAdapter != null) {
            mAdapter.stopListening();
        }
    }

    public void onFilter(Filters filters) {

        // Construct query basic query
        Query query = mFirestore.collection("favorites");

        // User
        query = query.whereEqualTo("userId", Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());

        // Update the query
        mQuery = query;
        mAdapter.setQuery(query);

        final ArrayList<String> favorites = new ArrayList<>();
        mQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                favorites.clear();
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                        Favorite favorite = document.toObject(Favorite.class);
                        String food = favorite.getName();
                        favorites.add(food);
                    }
                    updateWidgetData(favorites);
                }
            }
        });

        // Save filters
        mViewModel.setFilters(filters);
    }

    @Override
    public void onFavoriteSelected(DocumentSnapshot documentSnapshot) {
        Favorite favorite = documentSnapshot.toObject(Favorite.class);

        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailActivity.KEY_FOOD_ID, favorite.getFoodId());

        startActivity(intent);
    }

    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int scalingFactor = 200;
        int noOfColumns = (int) (dpWidth / scalingFactor);
        if (noOfColumns < 2)
            noOfColumns = 2;
        return noOfColumns;
    }
}
