package com.muchau.fomeanimal.adapter;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.muchau.fomeanimal.R;
import com.muchau.fomeanimal.model.Food;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Luis F. Muchau on 9/25/2018
 */
public class FoodAdapter extends FirestoreAdapter<FoodAdapter.ViewHolder> {

    public interface OnFoodSelectedListener {

        void onFoodSelected(DocumentSnapshot food);

    }

    private OnFoodSelectedListener mListener;

    public FoodAdapter(Query query, OnFoodSelectedListener listener) {
        super(query);
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater.inflate(R.layout.item_food, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(getSnapshot(position), mListener);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.foodImageView)
        ImageView foodImageView;

        @BindView(R.id.foodNameTextView)
        TextView foodNameTextView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final DocumentSnapshot snapshot,
                         final OnFoodSelectedListener listener) {

            Food food = snapshot.toObject(Food.class);
            Resources resources = itemView.getResources();

            // Load image
            Glide.with(foodImageView.getContext())
                    .load(food.getImage())
                    .error(R.drawable.error)
                    .placeholder(R.drawable.placeholder)
                    .into(foodImageView);

            foodNameTextView.setText(food.getName());

            // Click listener
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onFoodSelected(snapshot);
                    }
                }
            });
        }

    }
}
