package com.muchau.fomeanimal.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.muchau.fomeanimal.R;
import com.muchau.fomeanimal.model.Favorite;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Luis F. Muchau on 10/05/2018
 */
public class FavoriteAdapter extends FirestoreAdapter<FavoriteAdapter.ViewHolder> {

    public interface OnFavoriteSelectedListener {

        void onFavoriteSelected(DocumentSnapshot food);

    }

    private OnFavoriteSelectedListener mListener;

    public FavoriteAdapter(Query query, OnFavoriteSelectedListener listener) {
        super(query);
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater.inflate(R.layout.item_food, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(getSnapshot(position), mListener);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.foodImageView)
        ImageView foodImageView;

        @BindView(R.id.foodNameTextView)
        TextView foodNameTextView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final DocumentSnapshot snapshot,
                         final OnFavoriteSelectedListener listener) {

            Favorite favorite = snapshot.toObject(Favorite.class);

            // Load image
            Glide.with(foodImageView.getContext())
                    .load(favorite.getImage())
                    .error(R.drawable.error)
                    .placeholder(R.drawable.placeholder)
                    .into(foodImageView);

            foodNameTextView.setText(favorite.getName());

            // Click listener
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onFavoriteSelected(snapshot);
                    }
                }
            });
        }

    }
}
