package com.muchau.fomeanimal.model;

import android.text.TextUtils;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

/**
 * Created by Luis F. Muchau on 9/22/2018.
 */
public class Food {

    private String name;
    private String category;
    private String description;
    private String image;
    private long recommendations;
    private String location;
    private Restaurant restaurant;
    private String userId;
    private String userName;
    private @ServerTimestamp
    Date timestamp;

    public Food() {
    }

    public Food(String name, String category, String description, String image, long recommendations, String location, Restaurant restaurant, FirebaseUser user) {
        this.name = name;
        this.category = category;
        this.description = description;
        this.image = image;
        this.recommendations = recommendations;
        this.location = location;
        this.restaurant = restaurant;
        this.userId = user.getUid();
        this.userName = user.getDisplayName();
        if (TextUtils.isEmpty(this.userName)) {
            this.userName = user.getEmail();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(long recommendations) {
        this.recommendations = recommendations;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
