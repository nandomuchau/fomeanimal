package com.muchau.fomeanimal.model;

/**
 * Created by Luis F. Muchau on 9/22/2018.
 */
public class Restaurant {

    private String name;
    private String fullAddress;
    private String address;
    private String country;
    private String state;
    private String zipCode;
    private String city;

    public Restaurant() {}

    public Restaurant(String name, String fullAddress, String address, String country, String state, String zipCode, String city) {
        this.name = name;
        this.fullAddress = fullAddress;
        this.address = address;
        this.country = country;
        this.state = state;
        this.zipCode = zipCode;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
