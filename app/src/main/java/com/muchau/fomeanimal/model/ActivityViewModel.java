package com.muchau.fomeanimal.model;

import android.arch.lifecycle.ViewModel;

/**
 * Created by Luis F. Muchau on 9/24/2018
 */
public class ActivityViewModel extends ViewModel {

    private boolean mIsSigningIn;
    private Filters mFilters;

    public ActivityViewModel() {
        mIsSigningIn = false;
        mFilters = Filters.getDefault();
    }

    public boolean getIsSigningIn() {
        return mIsSigningIn;
    }

    public void setIsSigningIn(boolean mIsSigningIn) {
        this.mIsSigningIn = mIsSigningIn;
    }

    public Filters getFilters() {
        return mFilters;
    }

    public void setFilters(Filters mFilters) {
        this.mFilters = mFilters;
    }
}
