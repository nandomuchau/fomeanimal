package com.muchau.fomeanimal.model;

import android.text.TextUtils;

/**
 * Created by Luis F. Muchau on 9/24/2018.
 */
public class Filters {

    private String userId = null;
    private String category = null;
    private String location = null;

    public Filters() {}

    public static Filters getDefault() {
        Filters filters = new Filters();
        return filters;
    }

    public boolean hasUserId() {
        return !(TextUtils.isEmpty(userId));
    }

    public boolean hasCategory() {
        return !(TextUtils.isEmpty(category));
    }

    public boolean hasLocation() {
        return !(TextUtils.isEmpty(location));
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
